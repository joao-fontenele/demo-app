package migration

import (
	"database/sql"
	"errors"
	"fmt"
	"path"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/jackc/pgx/v5/stdlib"
	"github.com/spf13/viper"
)

type Service struct {
	Getwd func() (string, error)
}

func (s *Service) Goto(target int) error {
	db, m, err := s.newMigrate()
	if err != nil {
		return err
	}
	defer func() {
		_ = db.Close()
	}()
	err = m.Migrate(uint(target))
	if errors.Is(err, migrate.ErrNoChange) {
		return nil
	}
	return err
}

func (s *Service) Up() error {
	db, m, err := s.newMigrate()
	if err != nil {
		return err
	}
	defer func() {
		_ = db.Close()
	}()

	err = m.Up()
	if errors.Is(err, migrate.ErrNoChange) {
		return nil
	}
	return err
}

func (s *Service) newMigrate() (*sql.DB, *migrate.Migrate, error) {
	dir, err := s.Getwd()
	if err != nil {
		return nil, nil, fmt.Errorf("%w: could not get current directory", err)
	}

	dir = path.Join(dir, "sql", "migrations")
	db, err := sql.Open("pgx", viper.GetString("DB_URL"))
	if err != nil {
		return db, nil, fmt.Errorf("%w: failed to open DB connection", err)
	}

	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		return db, nil, fmt.Errorf("%w: failed to create migration driver", err)
	}

	m, err := migrate.NewWithDatabaseInstance("file://"+dir, "pgx", driver)
	if err != nil {
		return db, nil, fmt.Errorf("%w: failed to create a migration instance", err)
	}
	return db, m, nil
}

func (s *Service) Version() (uint, bool, error) {
	db, m, err := s.newMigrate()
	if err != nil {
		return 0, false, err
	}
	defer func() {
		_ = db.Close()
	}()
	return m.Version()
}
