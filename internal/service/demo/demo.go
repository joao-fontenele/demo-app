package demo

import (
	"context"
	"errors"
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/joao-fontenele/demo-app/internal/service/demo/store"
	errors2 "gitlab.com/joao-fontenele/demo-app/internal/service/errors"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/segmentio/ksuid"
)

var ErrVictim = errors.New("victim")

type Demo struct {
	color string
	store *store.Queries
}

func NewService(color string, conn *pgxpool.Pool) *Demo {
	return &Demo{
		color: color,
		store: store.New(conn),
	}
}

type ColorResponse struct {
	Color string
}

type Config struct {
	ID                  string
	LatencyMilliseconds int
	ErrorRate           int
}

func (r Config) Validate() error {
	if r.LatencyMilliseconds < 0 || r.LatencyMilliseconds > 1.2e+6 {
		return fmt.Errorf("%w: LatencyMilliseconds must gte 0 and lte 1.2+6", errors2.ErrBadRequest)
	}
	if r.ErrorRate < 0 || r.ErrorRate > 100 {
		return fmt.Errorf("%w: ErrorRate must be gte 0 and lte 100", errors2.ErrBadRequest)
	}

	return nil
}

func (d *Demo) Do(ctx context.Context, id string) (ColorResponse, error) {
	conf, err := d.GetOrCreateConfig(ctx, id)
	if err != nil {
		return ColorResponse{}, err
	}

	time.Sleep(time.Duration(conf.LatencyMilliseconds) * time.Millisecond)

	isErr := rand.Intn(100) < conf.ErrorRate
	if isErr {
		return ColorResponse{}, ErrVictim
	}
	return ColorResponse{Color: d.color}, nil
}

func (d *Demo) GetOrCreateConfig(ctx context.Context, id string) (Config, error) {
	var storeConf store.DemoConfig
	var conf Config
	err := pgx.ErrNoRows
	if id != "" {
		storeConf, err = d.store.GetConfig(ctx, id)
	}
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			conf, err = d.CreateConfig(ctx, ConfigArgs{
				LatencyMilliseconds: 0,
				ErrorRate:           0,
			})
			if err == nil {
				return conf, err
			}
		}

		return Config{}, err
	}

	return Config{
		ID:                  storeConf.ID,
		LatencyMilliseconds: int(storeConf.LatencyMilliseconds),
		ErrorRate:           int(storeConf.ErrorRate),
	}, nil
}

type ConfigArgs struct {
	LatencyMilliseconds int
	ErrorRate           int
}

func (d *Demo) CreateConfig(ctx context.Context, req ConfigArgs) (Config, error) {
	conf := Config{
		ID:                  ksuid.New().String(),
		LatencyMilliseconds: req.LatencyMilliseconds,
		ErrorRate:           req.ErrorRate,
	}
	if err := conf.Validate(); err != nil {
		return Config{}, err
	}

	_, err := d.store.CreateConfig(
		ctx,
		store.CreateConfigParams{
			ID:                  conf.ID,
			LatencyMilliseconds: int32(req.LatencyMilliseconds),
			ErrorRate:           int32(req.ErrorRate),
		},
	)
	if err != nil {
		return Config{}, err
	}

	return conf, nil
}

func (d *Demo) UpdateConfig(ctx context.Context, id string, req ConfigArgs) (Config, error) {
	conf := Config{
		ID:                  id,
		LatencyMilliseconds: req.LatencyMilliseconds,
		ErrorRate:           req.ErrorRate,
	}
	if err := conf.Validate(); err != nil {
		return Config{}, err
	}

	err := d.store.UpdateConfig(
		ctx,
		store.UpdateConfigParams{
			ID:                  conf.ID,
			LatencyMilliseconds: int32(req.LatencyMilliseconds),
			ErrorRate:           int32(req.ErrorRate),
		},
	)
	if err != nil {
		return Config{}, err
	}

	return conf, nil
}

func (d *Demo) DeleteConfig(ctx context.Context, id string) error {
	return d.store.DeleteConfig(ctx, id)
}
