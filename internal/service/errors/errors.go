package errors

import (
	errors2 "errors"
)

var ErrBadRequest = errors2.New("invalid request")
