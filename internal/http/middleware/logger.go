package middleware

import (
	"net/http"
	"time"

	"github.com/go-chi/chi/v5/middleware"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

func Logger() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			start := time.Now()
			// wrapping to register response data
			ww := middleware.NewWrapResponseWriter(w, r.ProtoMajor)

			defer func() {
				dict := zerolog.Dict()
				for k, v := range r.Header {
					if k == "Authorization" {
						v = []string{"REDACTED"}
					}
					dict.Strs(k, v)
				}
				log.Info().
					Int("status", ww.Status()).
					Int64("duration-ms", time.Since(start).Milliseconds()).
					Str("req-id", GetReqID(r.Context())).
					Str("method", r.Method).
					Str("path", r.RequestURI).
					Str("remoteAddr", r.RemoteAddr).
					Dict("headers", dict).
					Msg("http request ended")
			}()

			next.ServeHTTP(ww, r)
		})
	}
}
