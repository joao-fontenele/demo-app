package middleware

import (
	"net/http"
	"time"

	"github.com/segmentio/ksuid"
)

func Auth() func(handler http.Handler) http.Handler {
	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			sessionCookie, err := r.Cookie("sessionID")
			if err != nil || sessionCookie.Value == "" {
				http.SetCookie(w, &http.Cookie{
					//Secure: true, // TODO: dynamically set this to true
					Name:     "sessionID",
					Value:    ksuid.New().String(),
					Expires:  time.Now().Add(7 * 24 * time.Hour),
					HttpOnly: true,
					Path:     "/",
				})
			}

			handler.ServeHTTP(w, r)
		})
	}
}
