package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	errors2 "gitlab.com/joao-fontenele/demo-app/internal/service/errors"
)

func EncodeJSON[T any](w http.ResponseWriter, _ *http.Request, status int, v T, err error) {
	w.Header().Set("Content-Type", "application/json")
	if err != nil {
		if ok := errors.Is(err, errors2.ErrBadRequest); ok {
			w.WriteHeader(http.StatusBadRequest)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
		}
		_, _ = fmt.Fprintf(w, `{"message": "%s"}`, err.Error())
		return
	}

	if err = json.NewEncoder(w).Encode(v); err != nil {
		_, _ = fmt.Fprintf(w, `{"message": "failed to json encode the response"}`)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func DecodeJSON[T any](w http.ResponseWriter, r *http.Request) (T, error) {
	var v T
	err := json.NewDecoder(r.Body).Decode(&v)
	_ = r.Body.Close()
	return v, err
}
