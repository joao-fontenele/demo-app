package handler

import (
	"fmt"
	"net/http"
)

type Internal struct {
}

func (h Internal) Health(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	_, _ = fmt.Fprint(w, `{"healthy": "true"}`)
}
