package handler

import (
	"context"
	"encoding/json"
	"net/http"

	echopb "gitlab.com/joao-fontenele/demo-app/proto"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type Echo struct {
}

func (h Echo) Echo(w http.ResponseWriter, r *http.Request) {
	var req echopb.EchoRequest
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}
	conn, err := grpc.NewClient(":50051", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		http.Error(w, "Service unavailable", http.StatusServiceUnavailable)
		return
	}
	defer func() {
		_ = conn.Close()
	}()
	client := echopb.NewEchoServiceClient(conn)
	res, err := client.Echo(context.Background(), &req)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	_ = json.NewEncoder(w).Encode(res)
}
