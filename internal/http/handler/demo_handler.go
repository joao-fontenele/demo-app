package handler

import (
	"net/http"

	"gitlab.com/joao-fontenele/demo-app/internal/http/api"
	mw "gitlab.com/joao-fontenele/demo-app/internal/http/middleware"
	"gitlab.com/joao-fontenele/demo-app/internal/service/demo"

	"github.com/go-chi/chi/v5"
)

type Demo struct {
	DemoService DemoService
}

type DemoAPIRequest struct {
	LatencyMilliseconds int `json:"latencyMilliseconds"`
	ErrorRate           int `json:"errorRate"`
}

type DemoAPIResponse struct {
	Color string `json:"color"`
}

func (d Demo) DemoAPI(w http.ResponseWriter, r *http.Request) {
	req, err := api.DecodeJSON[DemoAPIRequest](w, r)
	if err != nil {
		api.EncodeJSON(w, r, http.StatusBadRequest, req, err)
		return
	}

	resp, err := d.DemoService.Do(r.Context(), mw.GetReqID(r.Context()))

	api.EncodeJSON(w, r, http.StatusOK, resp, err)
}

type DemoUpdateConfigRequest struct {
	ID                  string `json:"id"`
	LatencyMilliseconds int    `json:"latencyMilliseconds"`
	ErrorRate           int    `json:"errorRate"`
}

type DemoCreateConfigRequest struct {
	LatencyMilliseconds int `json:"latencyMilliseconds"`
	ErrorRate           int `json:"errorRate"`
}

type ConfigResponse struct {
	ID                  string `json:"id"`
	LatencyMilliseconds int    `json:"latencyMilliseconds"`
	ErrorRate           int    `json:"errorRate"`
}

func (d Demo) UpdateConfig(w http.ResponseWriter, r *http.Request) {
	req, err := api.DecodeJSON[DemoUpdateConfigRequest](w, r)
	if err != nil {
		api.EncodeJSON(w, r, http.StatusBadRequest, req, err)
	}

	conf, err := d.DemoService.UpdateConfig(r.Context(), chi.URLParam(r, "id"), demo.ConfigArgs{
		LatencyMilliseconds: req.LatencyMilliseconds,
		ErrorRate:           req.ErrorRate,
	})

	resp := ConfigResponse{
		ID:                  conf.ID,
		LatencyMilliseconds: conf.LatencyMilliseconds,
		ErrorRate:           conf.ErrorRate,
	}

	api.EncodeJSON(w, r, http.StatusOK, resp, err)
}

func (d Demo) GetOrCreateConfig(w http.ResponseWriter, r *http.Request) {
	conf, err := d.DemoService.GetOrCreateConfig(r.Context(), mw.GetReqID(r.Context()))

	resp := ConfigResponse{
		ID:                  conf.ID,
		LatencyMilliseconds: conf.LatencyMilliseconds,
		ErrorRate:           conf.ErrorRate,
	}

	api.EncodeJSON(w, r, http.StatusOK, resp, err)
}

func (d Demo) CreateConfig(w http.ResponseWriter, r *http.Request) {
	req, err := api.DecodeJSON[DemoCreateConfigRequest](w, r)
	if err != nil {
		api.EncodeJSON(w, r, http.StatusBadRequest, req, err)
	}

	conf, err := d.DemoService.CreateConfig(r.Context(), demo.ConfigArgs{
		LatencyMilliseconds: req.LatencyMilliseconds,
		ErrorRate:           req.ErrorRate,
	})

	resp := ConfigResponse{
		ID:                  conf.ID,
		LatencyMilliseconds: conf.LatencyMilliseconds,
		ErrorRate:           conf.ErrorRate,
	}

	api.EncodeJSON(w, r, http.StatusOK, resp, err)
}

func (d Demo) DeleteConfig(w http.ResponseWriter, r *http.Request) {
	err := d.DemoService.DeleteConfig(r.Context(), chi.URLParam(r, "id"))
	api.EncodeJSON(w, r, http.StatusOK, struct{}{}, err)
}
