package handler

import (
	"context"
	"net/http"

	mw "gitlab.com/joao-fontenele/demo-app/internal/http/middleware"
	"gitlab.com/joao-fontenele/demo-app/internal/service/demo"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

type DemoService interface {
	Do(ctx context.Context, id string) (demo.ColorResponse, error)
	GetOrCreateConfig(ctx context.Context, id string) (demo.Config, error)
	CreateConfig(ctx context.Context, req demo.ConfigArgs) (demo.Config, error)
	UpdateConfig(ctx context.Context, id string, req demo.ConfigArgs) (demo.Config, error)
	DeleteConfig(ctx context.Context, id string) error
}

var _ DemoService = &demo.Demo{}

func NewRouter(demo DemoService) http.Handler {
	r := chi.NewRouter()

	r.Use(mw.RequestID())
	r.Use(mw.Logger())

	r.Mount("/debug", middleware.Profiler())

	r.Get("/internal/healthz", (Internal{}).Health)
	r.Post("/echo", (Echo{}).Echo)

	r.Route("/api/demo/config", func(r chi.Router) {
		demoHandler := Demo{DemoService: demo}

		r.Get("/{id}/request", demoHandler.DemoAPI)
		r.Get("/{id}", demoHandler.GetOrCreateConfig)
		r.Post("/{id}", demoHandler.CreateConfig)
		r.Put("/{id}", demoHandler.UpdateConfig)
		r.Delete("/{id}", demoHandler.DeleteConfig)
	})

	return r
}
