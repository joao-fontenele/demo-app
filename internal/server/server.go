package server

import (
	"context"
	"net"
	"net/http"
	"os"

	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/joao-fontenele/demo-app/internal/service/demo"

	grpc2 "gitlab.com/joao-fontenele/demo-app/internal/grpc"
	"gitlab.com/joao-fontenele/demo-app/internal/http/handler"
	echopb "gitlab.com/joao-fontenele/demo-app/proto"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/rs/zerolog/pkgerrors"
	"github.com/spf13/viper"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type Server struct {
	grpc2.Router

	color string

	dbPool *pgxpool.Pool
}

func New(color string, pool *pgxpool.Pool) *Server {
	return &Server{
		color:  color,
		dbPool: pool,
	}
}

func (s *Server) Start(ctx context.Context) error {
	s.Configure()
	log.Info().Str("version", viper.GetString("CommitHash")).Msg("")
	errgrp, _ := errgroup.WithContext(ctx)

	demoService := demo.NewService("green", s.dbPool)

	errgrp.Go(func() error {
		grpcServer := grpc.NewServer()
		echopb.RegisterEchoServiceServer(grpcServer, s)
		reflection.Register(grpcServer)
		lis, err := net.Listen("tcp", ":50051")
		if err != nil {
			return err
		}

		log.Info().Str("port", lis.Addr().String()).Msg("gRPC server listening on")
		return grpcServer.Serve(lis)
	})

	errgrp.Go(func() error {
		log.Info().Msg("HTTP server listening on :8080")
		return http.ListenAndServe(":8080", handler.NewRouter(demoService))
	})

	return errgrp.Wait()
}

func (s *Server) Configure() {
	if viper.GetString("CommitHash") == "local" {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	}

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if viper.IsSet("logLevel") {
		switch viper.GetString("logLevel") {
		case "debug":
			zerolog.SetGlobalLevel(zerolog.DebugLevel)
		case "info":
			zerolog.SetGlobalLevel(zerolog.InfoLevel)
		case "warn":
			zerolog.SetGlobalLevel(zerolog.WarnLevel)
		case "error":
			zerolog.SetGlobalLevel(zerolog.ErrorLevel)
		}
	}

	zerolog.ErrorStackMarshaler = pkgerrors.MarshalStack

	log.Logger = log.With().Timestamp().Caller().Logger()
}
