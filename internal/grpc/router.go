package grpc

import (
	"context"

	"github.com/rs/zerolog/log"
	echopb "gitlab.com/joao-fontenele/demo-app/proto"
)

type Router struct {
	echopb.UnimplementedEchoServiceServer
}

func (r *Router) Echo(ctx context.Context, req *echopb.EchoRequest) (*echopb.EchoResponse, error) {
	log.Info().Msg("grpc")
	return &echopb.EchoResponse{Message: req.Message}, nil
}
