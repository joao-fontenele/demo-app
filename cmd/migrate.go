package cmd

import (
	"os"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/joao-fontenele/demo-app/internal/service/migration"
)

var migrationTarget int

// migrateCmd represents the migrate command
var migrateCmd = &cobra.Command{
	Use:   "migrate",
	Short: "Run database migration commands",
	Long: `Run database migration commands:

$ app migrate up
$ app migrate goto -t <target>
$ app migrate version`,
	ValidArgs: []string{"up", "goto", "version"},
	Args:      cobra.MatchAll(cobra.ExactArgs(1), cobra.OnlyValidArgs),
	Run: func(cmd *cobra.Command, args []string) {
		s := migration.Service{
			Getwd: os.Getwd,
		}
		var err error
		switch args[0] {
		case "up":
			err = s.Up()
		case "goto":
			err = s.Goto(migrationTarget)
		case "version":
			var version uint
			var dirty bool
			version, dirty, err = s.Version()
			log.Info().Uint("version", version).Bool("dirty", dirty).Msg("migrations state")
		}
		if err != nil {
			log.Fatal().Err(err).Msg("failed")
		}
		log.Info().Msg("done")
	},
}

func init() {
	rootCmd.AddCommand(migrateCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// migrateCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	migrateCmd.Flags().IntVarP(
		&migrationTarget,
		"target",
		"t",
		0,
		"required target number of the migration",
	)
}
