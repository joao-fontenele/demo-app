package main

import (
	"gitlab.com/joao-fontenele/demo-app/cmd"
)

func main() {
	cmd.Execute()
}
