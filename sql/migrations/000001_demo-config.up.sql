CREATE TABLE demo_config (
    id CHAR(27) PRIMARY KEY,
    latency_milliseconds INTEGER,
    error_rate INTEGER
);
