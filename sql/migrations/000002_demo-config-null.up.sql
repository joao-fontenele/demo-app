ALTER TABLE demo_config
    ALTER COLUMN id SET NOT NULL,
    ALTER COLUMN latency_milliseconds SET NOT NULL,
    ALTER COLUMN error_rate SET NOT NULL;

ALTER TABLE demo_config
    ALTER COLUMN id TYPE CHAR(27) COLLATE "C";
