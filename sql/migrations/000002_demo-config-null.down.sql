ALTER TABLE demo_config
    ALTER COLUMN id DROP NOT NULL,
    ALTER COLUMN latency_milliseconds DROP NOT NULL,
    ALTER COLUMN error_rate DROP NOT NULL;

ALTER TABLE demo_config
    ALTER COLUMN id TYPE VARCHAR(27) COLLATE "default";
