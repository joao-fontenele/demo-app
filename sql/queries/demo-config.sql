-- name: GetConfig :one
SELECT * FROM demo_config
WHERE id = $1 LIMIT 1;

-- name: CreateConfig :one
INSERT INTO demo_config (
    id, latency_milliseconds, error_rate
) VALUES (
    $1, $2, $3
)
RETURNING *;

-- name: UpdateConfig :exec
UPDATE demo_config
SET latency_milliseconds = $2,
    error_rate = $3
WHERE id = $1;

-- name: DeleteConfig :exec
DELETE FROM demo_config
WHERE id = $1;
