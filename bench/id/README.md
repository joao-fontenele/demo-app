# KSUID and ULID

benchmarks:

```
goos: linux
goarch: amd64
pkg: id
cpu: 12th Gen Intel(R) Core(TM) i7-1260P
BenchmarkStringifyKSUID-16         2101911        476.3 ns/op    32 B/op    1 allocs/op
BenchmarkStringifyULID-16         12859327        95.45 ns/op    48 B/op    2 allocs/op
BenchmarkParseKSUID/KSUID-16    1000000000    0.0000003 ns/op     0 B/op    0 allocs/op
BenchmarkParseULID/ULID-16      1000000000    0.0000001 ns/op     0 B/op    0 allocs/op
```

KSUID is almost 5x slower than ULID for generating string IDs, however it makes fewer allocations.
