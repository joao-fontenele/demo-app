module id

go 1.22

require (
	github.com/oklog/ulid/v2 v2.1.0
	github.com/segmentio/ksuid v1.0.4
)
