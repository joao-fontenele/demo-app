package main

import (
	"fmt"

	"github.com/oklog/ulid/v2"
	"github.com/segmentio/ksuid"
)

func main() {
	k := ksuid.New()
	ks := k.String()
	fmt.Printf("%s %d\n", ks, len(ks))

	u := ulid.Make()
	us := u.String()
	fmt.Printf("%s %d\n", us, len(us))
}
