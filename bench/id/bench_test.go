package main_test

import (
	"testing"

	"github.com/oklog/ulid/v2"
	"github.com/segmentio/ksuid"
)

var result string
var ksid ksuid.KSUID
var uid ulid.ULID
var gErr error

var ksidTestData []string
var uidTestData []string

func BenchmarkStringifyKSUID(b *testing.B) {
	var id string
	for _ = range b.N {
		id = ksuid.New().String()
	}
	result = id
}

func BenchmarkStringifyULID(b *testing.B) {
	var id string
	for _ = range b.N {
		id = ulid.Make().String()
	}
	result = id
}

func BenchmarkParseKSUID(b *testing.B) {
	var id ksuid.KSUID
	var err error
	for i := range b.N {
		ksidTestData = append(ksidTestData, ksuid.New().String())
		b.Run("KSUID", func(b *testing.B) {
			id, err = ksuid.Parse(ksidTestData[i])
			if err != nil {
				b.Fail()
			}
		})
	}
	gErr = err
	ksid = id
}

func BenchmarkParseULID(b *testing.B) {
	var id ulid.ULID
	var err error
	for i := range b.N {
		uidTestData = append(uidTestData, ulid.Make().String())
		b.Run("ULID", func(b *testing.B) {
			id, err = ulid.Parse(uidTestData[i])
			if err != nil {
				b.Fail()
			}
		})
	}
	gErr = err
	uid = id
}
