FROM golang:1.22 as builder

ENV APP /app

RUN mkdir -p ${APP}
WORKDIR ${APP}

COPY go.mod go.sum ${APP}/
RUN go mod download

COPY . ${APP}/
RUN CGO_ENABLED=0 go build -ldflags="-s -w -X gitlab.com/joao-fontenele/demo-app/cmd.CommitHash=$(git rev-parse HEAD)" -o bin/app main.go

FROM gcr.io/distroless/base

ENV APP /app
WORKDIR ${APP}

COPY ./sql/migrations/ ${APP}/sql/migrations/
COPY --from=builder ${APP}/app ${APP}/app

CMD ["/app/app", "server"]
