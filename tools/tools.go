//go:build tools
// +build tools

package tools

// see:
// - https://github.com/go-modules-by-example/index/blob/master/010_tools/README.md
// - https://github.com/MarioCarrion/todo-api-microservice-example/blob/e44dc3d4016b80b8102eb1214fea452aebdd2667/internal/tools/tools.go

import ( // add new tools below
	_ "github.com/cespare/reflex"
	_ "github.com/go-delve/delve/cmd/dlv"
	_ "github.com/segmentio/ksuid/cmd/ksuid"
	_ "github.com/spf13/cobra-cli"
	_ "github.com/sqlc-dev/sqlc/cmd/sqlc"
	_ "github.com/ykadowak/zerologlint/cmd/zerologlint"
	_ "golang.org/x/tools/cmd/goimports"
	_ "golang.org/x/vuln/cmd/govulncheck"
	_ "google.golang.org/grpc/cmd/protoc-gen-go-grpc"
	_ "google.golang.org/protobuf/cmd/protoc-gen-go"
)
