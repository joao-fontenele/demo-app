# Get the currently used golang install path (in GOPATH/bin, unless GOBIN is set)
ifeq (,$(shell go env GOBIN))
GOBIN=${PWD}/bin
else
GOBIN=$(shell go env GOBIN)
endif

# Setting SHELL to bash allows bash commands to be executed by recipes.
# Options are set to exit when a recipe line exits non-zero or a piped command fails.
SHELL = /usr/bin/env bash -o pipefail
.SHELLFLAGS = -ec

all: run-docker

# Location to install dependencies to
LOCALBIN ?= $(shell pwd)/bin
$(LOCALBIN):
	mkdir -p $(LOCALBIN)

.PHONY: proto
proto:
	$(LOCALBIN)/protoc --go_out=. --go_opt=paths=source_relative \
        --go-grpc_out=. --go-grpc_opt=paths=source_relative \
        proto/echo.proto

.PHONY: sqlc
sqlc:
	rm -fr ./internal/repo
	$(LOCALBIN)/sqlc generate

.PHONY: generate
generate: proto sqlc
	make fmt || echo formatted, all is good

.PHONY: install-tools
install-tools: $(LOCALBIN)
	cd tools ; PATH=${PATH}:${GOBIN} GOBIN=${GOBIN} ./install.sh

.PHONY: dlv
dlv: gomodtidy database
	CGO_ENABLED=0 go build -gcflags="all=-N -l" -o ./bin/app main.go
	@echo connect to the debugger from the IDE
	$(LOCALBIN)/dlv --listen=:2345 --headless=true --api-version=2 --accept-multiclient exec ./bin/app -- server

.PHONY: run
run: gomodtidy database
	$(LOCALBIN)/reflex -d none -s -r '\.go$$' go run main.go server

.PHONY: database
database:
	docker compose up -d

.PHONY: build
build:
	builder=$(shell docker buildx create --use)
	docker buildx build --push --platform linux/arm64/v8,linux/amd64 --tag joao-fontenele/demo-app:$(shell git rev-parse HEAD) .
	docker buildx rm $(builder)

.PHONY: run-docker
run-docker: build
	docker run --rm -p "8080:8080" -p "50051:50051" -it joao-fontenele/demo-app:latest

.PHONY: psql
psql:
	docker compose exec -it postgres /bin/sh -c "PGPASSWORD=1335555 psql -h localhost -U demo"

.PHONY: fmt
fmt:
	@if goimports -l -w . | grep . ; then \
		echo "goimports found files that need to be formatted"; \
		exit 1; \
	fi

.PHONY: lint
lint:
	$(LOCALBIN)/golangci-lint run ./...
	go vet -vettool=`which zerologlint` ./...
	go vet ./...

.PHONY: test
test:
	go test ./...

.PHONY: gomodtidy
gomodtidy:
	go mod tidy

.PHONY: govulncheck
govulncheck:
	$(LOCALBIN)/govulncheck ./...

.PHONY: ci
ci: install-tools fmt lint test
